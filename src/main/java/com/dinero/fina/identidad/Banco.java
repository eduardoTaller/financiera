package com.dinero.fina.identidad;





import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;









@Entity
@Table(name="banco")

public class Banco {
	private Long id;
	private String nombre;
	
	@OneToMany(mappedBy="banco",fetch=FetchType.LAZY)
	private Set<Cuenta> cuenta;
	
	public Banco() {
	}

	public Banco(Long id, String nombre) {
		this.id = id;
		this.nombre = nombre;
		
	}
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
