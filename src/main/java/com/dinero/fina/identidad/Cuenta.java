package com.dinero.fina.identidad;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "cuenta")

public class Cuenta {
	private Long id;
	private Integer nro_cuenta;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "banco_id", nullable = false)
	private Banco banco;

	public Cuenta() {
	}

	public Cuenta(Long id, Integer nro_cuenta, Banco banco) {
		this.id = id;
		this.nro_cuenta = nro_cuenta;
		this.banco = banco;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	public Long getId() {
		return id;
	}

	public Integer getNro_cuenta() {
		return nro_cuenta;
	}

	public void setNro_cuenta(Integer nro_cuenta) {
		this.nro_cuenta = nro_cuenta;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

}
